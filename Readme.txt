

Instructions
Double-click SCardToolSet.exe to open the program.

You might wonder how do I even get to use this program. You will need a smart card reader/writer in order to make use of this program. You can find cheap ones on aliexpress, ebay or amazon for $15-$20. Just search for "ISO-7816" and you should find some readers/writers for this type of card. For cards you will need ISO-7816 type of blank cards. Again these can be found on aliexpress/ebay/amazon aswell. Connect your smart card reader, make sure you install drivers if it has them, open the program, select the smart card reader from the Service list and click Connect on the right of the drop-down list.


Happy Carding!

